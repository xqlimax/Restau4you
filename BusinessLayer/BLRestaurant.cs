using Restau4You.DataAccessLayer;
using Restau4You.CustomError;
using System.Collections.Generic;
using Restau4You.Models;

namespace Restau4You.BusinessLayer
{
    public class BLRestaurant
    {
        public static List<Restaurant> RestaurantList(int idGroup)
        {
            try
            {
                List<Restaurant> data = DALRestaurant.RestaurantList(idGroup);
                return data;
            }
            catch (ErrorDataBaseConnection ex)
            {
                throw ex;
            }
        }
    }
}