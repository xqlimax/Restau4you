using System.Collections.Generic;
using Restau4You.CustomError;
using Restau4You.DataAccessLayer;
using Restau4You.Models;

namespace Restau4You.BusinessLayer
{
    public class BLGroup
    {
        public static List<Group> GroupList()
        {
            try
            {
                List<Group> data = DALGroup.GroupList();
                return data;
            }
            catch (ErrorDataBaseConnection ex)
            {
                throw ex;
            }
        }

                public static List<GroupDish> GroupDish(int idGroup)
        {
            try
            {
                List<GroupDish> data = DALGroup.GroupDish(idGroup);
                return data;
            }
            catch (ErrorDataBaseConnection ex)
            {
                throw ex;
            }
        }
    }
}