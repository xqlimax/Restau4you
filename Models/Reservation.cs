using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restau4You.Models
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public Guid ReservationNumber { get; set; }

        [Required]
        public char Status { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int PeopleNumber { get; set; }

        [Required]
        public int TotalPrice { get; set; }

        [ForeignKey("Client")]
        public int ClientId { get; set; }
        public Client Client { get; set; }

        [ForeignKey("Restaurant")]
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
        
        [ForeignKey("Table")]
        public int TableId { get; set; }
        public Table Table { get; set; }

    }
}