using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restau4You.Models
{
    public class RestaurantDish
    {
        [Required]
        public int RestaurantPrice { get; set; }

        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }

    }
}