namespace Restau4You.Models
{
    public class PersonDish
    {
        
        public int PersonId { get; set; }
        public Person Person { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }
    }
}