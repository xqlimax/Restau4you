using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Restau4You.Models
{
    public class Type
    {
        [Key]
        public int Id { get; set; }
        
        [Required, MaxLength(128)]
        public string Description { get; set; }
        public ICollection<Dish> Plats { get; set; }
    }
}