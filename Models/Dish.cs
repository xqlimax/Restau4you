using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restau4You.Models
{
    public class Dish
    {
        [Key]
        public int Id { get; set; }
        
        [Required, MaxLength(256)]
        public string Description { get; set; }

        [ForeignKey("Type")]
        public int TypeId { get; set; }
        public Type Type { get; set; }

        public ICollection<RestaurantDish> RestaurantDishs { get; set; }
        public ICollection<GroupDish> GroupDishs { get; set; }
        public ICollection<MenuDish> MenuDishs { get; set; }
        public ICollection<PersonDish> PersonDish { get; set; }

    }
}