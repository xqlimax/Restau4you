using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restau4You.Models
{
    public class Group
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        
        [Required, MaxLength(128)]
        public string Name { get; set; }

        public ICollection<Restaurant> Restaurants { get; set; }

        public ICollection<GroupDish> GroupDishs { get; set; }

    }
}
