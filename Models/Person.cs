using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Restau4You.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public char ALaCarte { get; set; }
        [ForeignKey("Menu")]
        public int? MenuId { get; set; }
        public Menu Menu { get; set; }
        [ForeignKey("Reservation")]
        public int ReservationId { get; set; }
        public Reservation Reservation { get; set; }
        public ICollection<PersonDish> PersonDish { get; set; }

    }
}