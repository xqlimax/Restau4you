using System.Collections.Generic;
using Restau4You.CustomError;

namespace Restau4You.Models.ViewModels
{
    public class GroupRestaurant
    {
        public List<Group> Groups = new List<Group>();
        public List<Restaurant> Restaurants = new List<Restaurant>();

        public ErrorDataBaseConnection Error = new ErrorDataBaseConnection(0);
    }
}