using System;
using System.ComponentModel.DataAnnotations;

namespace Restau4You.Models
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        [Required, MaxLength(255)]
        public string Name { get; set; }
        [Required, MaxLength(255)]
        public string FirstName {get; set;}
        [Required]
        public int Age { get; set; }
        [Required]
        public Guid ClientNumber { get; set; }
    }
}