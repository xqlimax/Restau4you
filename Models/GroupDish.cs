using System.ComponentModel.DataAnnotations;

namespace Restau4You.Models
{
    public class GroupDish
    {
        [Required]
        public int GroupMinimumPrice { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }
    }
}