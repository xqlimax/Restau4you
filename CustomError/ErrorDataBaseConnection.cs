using System;

namespace Restau4You.CustomError
{
    public class ErrorDataBaseConnection : Exception
    {
        private string _Message;
        private int _Number;

        public ErrorDataBaseConnection(int pNumber)
        {
            _Number = pNumber;
            switch (pNumber)
            {
                case 4060:
                    _Message = "Mauvaise DB";
                    break;
                case 18456:
                    _Message = "Mauvais User";
                    break;
                default:
                    _Message = "Message non connu..." + _Number;
                    break;
            }
        }

        public int Number
        {
            get
            {
                return _Number;
            }

        }

        public override string Message
        {
            get
            {
                return _Message;
            }

        }

    }
}