using System;

namespace Restau4You.CustomError
{
    public class ErrorRestaurant : Exception
    {
        private string _Message;
        private int _Number;

        public ErrorRestaurant(int pNumber)
        {
            _Number = pNumber;
            switch (pNumber)
            {
                case 0:
                    _Message = "Test";
                    break;
                default:
                    _Message = "Message non connu..." + _Number;
                    break;
            }
        }

        public int Number
        {
            get
            {
                return _Number;
            }

        }

        public override string Message
        {
            get
            {
                return _Message;
            }

        }
    }
}