using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Restau4You.CustomError;
using Dapper;
using Restau4You.Models;
using System.Linq;

namespace Restau4You.DataAccessLayer
{
    public class DALRestaurant
    {
        public static List<Restaurant> RestaurantList(int idGroup)
        {
            using (var connection = new SqlConnection())
            {
                connection.ConnectionString = Utils.ConnectionString;
                try
                {
                    connection.Open();
                    List<Restaurant> restaurants = connection.Query<Restaurant>("RestaurantList", new { groupId = idGroup } ,commandType: CommandType.StoredProcedure).ToList();
                    return restaurants;
                }
                catch (SqlException ex)
                {
                    throw new ErrorDataBaseConnection(ex.Number);
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }
    }
}
