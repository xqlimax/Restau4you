using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Restau4You.CustomError;
using Restau4You.Models;

namespace Restau4You.DataAccessLayer
{
    public class DALGroup
    {
        public static List<Group> GroupList()
        {
            using (var connection = new SqlConnection())
            {
                connection.ConnectionString = Utils.ConnectionString;
                try
                {
                    connection.Open();
                    List<Group> groups = connection.Query<Group>("GroupList", commandType: CommandType.StoredProcedure).ToList();
                    return groups;
                }
                catch (SqlException ex)
                {
                    throw new ErrorDataBaseConnection(ex.Number);
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }

        public static List<GroupDish> GroupDish(int idGroup)
        {
            using (var connection = new SqlConnection())
            {
                connection.ConnectionString = Utils.ConnectionString;
                try
                {
                    connection.Open();
                    List<GroupDish> groupDish = connection.Query<GroupDish>("GetGroupDish", new { groupId = idGroup }, commandType: CommandType.StoredProcedure).ToList();
                    return groupDish;
                }
                catch (SqlException ex)
                {
                    throw new ErrorDataBaseConnection(ex.Number);
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

            }
        }
    }
}