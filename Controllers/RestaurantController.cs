using Microsoft.AspNetCore.Mvc;
using Restau4You.BusinessLayer;
using Restau4You.CustomError;
using System.Data;
using System.Collections.Generic;
using Restau4You.Models;
using Restau4You.Models.ViewModels;
using System.Net;
using System;

namespace Restau4You.Controllers
{
    [Route("Restaurant")]
    public class RestaurantController : Controller
    {
        [Route("")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [Route("getGroup")]
        [HttpGet]
        public JsonResult GetGroup()
        {
            var json = new List<Group>();
            try
            {
                json = BLGroup.GroupList();
            }
            catch (ErrorDataBaseConnection ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
            return Json(json);
        }

        [Route("getRestaurant")]
        [HttpGet]
        public JsonResult GetRestaurant(int idGroup)
        {
            var json = new List<Restaurant>();
            try
            {
                json = BLRestaurant.RestaurantList(idGroup);
            }
            catch (ErrorDataBaseConnection ex)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(ex.Message);
            }
            return Json(json);
        }

        [Route("getDetails")]
        [HttpGet]
        public IActionResult GetDetails(int Restaurant)
        {
            return View();
        }
        
        [Route("changeDish")]
        [HttpGet]
        public IActionResult ChangeDish(int idGroup, int idRestaurant)
        {
            BLGroup.GroupDish()
            return View();
        }
        
        [Route("changeMenu")]
        [HttpGet]
        public IActionResult ChangeMenu(int Restaurant)
        {
            // try{

            // }
            // catch(ErrorDataBaseConnection ex)
            // {
            //     return
            // }
            return View();
        }

    }
}