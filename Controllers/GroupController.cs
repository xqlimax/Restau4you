using Microsoft.AspNetCore.Mvc;

namespace Restau4You.Controllers
{
    [Route("Group")]
    public class GroupController : Controller
    {
        [Route("")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}