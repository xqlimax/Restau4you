using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Restau4You.DataAccessLayer;

namespace Restau4you.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        [Route("")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [Route("Redirect")]
        [HttpGet]

        public IActionResult Redirect()
        {
            return RedirectToAction("Index", "Home");
        }
    }
}