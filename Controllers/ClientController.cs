using Microsoft.AspNetCore.Mvc;

namespace Restau4You.Controllers
{
    [Route("Client")]
    public class ClientController : Controller
    {
        [Route("")]
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

    }
}