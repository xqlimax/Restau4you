using System;
using Microsoft.AspNetCore.Mvc;

namespace Restau4You.Controllers
{
    [Route("Error")]
    public class ErrorController : Controller
    {
        [Route("{errCode}")]
        [HttpGet]
        public IActionResult Errors(string errCode)
        {
            return View($"~/Views/Shared/Error/{errCode}.cshtml");
        }
    }
}