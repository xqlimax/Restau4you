using System.Linq;
using Microsoft.EntityFrameworkCore;
using Restau4You.Models;

namespace Restau4You.Data
{
    public class Restau4YouDbContext : DbContext
    {
        public Restau4YouDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //init all the many to many table
            modelBuilder.Entity<RestaurantDish>()
                .HasKey(t => new { t.RestaurantId, t.DishId });

            modelBuilder.Entity<GroupDish>()
            .HasKey(t => new { t.GroupId, t.DishId });

            modelBuilder.Entity<MenuDish>()
            .HasKey(t => new { t.MenuId, t.DishId });

            modelBuilder.Entity<PersonDish>()
            .HasKey(t => new { t.PersonId, t.DishId });
            
            //Unique key for GUID
            modelBuilder.Entity<Client>()
            .HasAlternateKey(c => c.ClientNumber)
            .HasName("AlternateKey_ClientNumber");

            modelBuilder.Entity<Reservation>()
            .HasAlternateKey(c => c.ReservationNumber)
            .HasName("AlternateKey_ReservationNumber");

            //a table needs a key this define a key date-restaurantId for the 
            //SalesRevenue table
            modelBuilder.Entity<SalesRevenue>()
            .HasKey(t => new { t.Date, t.RestaurantId});
            //Workaround for multiple cascade deletion
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
            .SelectMany(t => t.GetForeignKeys())
            .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }
        public DbSet<Restaurant> Restaurant { get; set; }
        public DbSet<Group> Group { get; set; }
        public DbSet<Dish> Dish { get; set; }
        public DbSet<Type> Type { get; set; }
        public DbSet<Table> Table { get; set; }
        public DbSet<RestaurantDish> RestaurantDish { get; set; }
        public DbSet<GroupDish> GroupDish { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<MenuDish> MenuDish { get; set; }
        public DbSet<Person> Person { get; set; }
        public DbSet<PersonDish> PersonDish { get; set; }
        public DbSet<SalesRevenue> SalesRevenue { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Reservation> Reservation { get; set; }

    }
}