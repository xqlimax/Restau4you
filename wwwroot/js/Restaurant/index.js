$().ready(
  $.ajax({
    dataType: "json",
    url: "Restaurant/getGroup",
    success: (data) => {
      for (let i = 0; i < data.length; i++) {
        $("#groupSelect").append(
          '<option value="' + data[i].id + '">' + data[i].name + "</option>"
        );
      }
      let groupSelect = $("#groupSelect");
      getRestaurant(groupSelect.val());
      groupSelect.on("change", function (e) {
        getRestaurant(groupSelect.val());
      });
    },
    error: (error) => {
      alert(error.responseText);
    }
  })
);

function getRestaurant(idGroup) {
  $.ajax({
    dataType: "json",
    url: "Restaurant/getRestaurant",
    data: {
      idGroup
    },
    success: (data) => {
      let restaurantSelect = $('#restaurantSelect');
      restaurantSelect.html(null);
      if (data.length === 0) {
        let option = '<option>Aucun résultat</option>';
        restaurantSelect.append(option);
      } else {
        for (let i = 0; i < data.length; i++) {
          let option = '<option value="' + data[i].id + '">' + data[i].name + '</option>';
          restaurantSelect.append(option);
        }
      }
    },
    error: (error) => {
      alert(error.responseText);
    }
  })
}